var imgs = [
  "img/bald.png",
  "img/bindi.png",
  "img/cheers.png",
  "img/couple.png",
  "img/crisis.png",
  "img/delicious.png",
  "img/drinkup.png",
  "img/flavour.png",
  "img/fresh.png",
  "img/giant.png",
  "img/gossip.png",
  "img/morningcup.png",
  "img/mrsbrimble.png",
  "img/ok.png",
  "img/quality.png",
  "img/rooster.png",
  "img/yesyes.png"
];

var randnum = Math.floor(Math.random() * imgs.length);
var img = imgs[randnum];
var imgstr = '<img class="img-fluid rounded" src="' + img + '" alt="Jolly Good!" id="jgPic">';

document.write(imgstr);
document.close();
